'use strict'
window.addEventListener('load', function(){
    console.log("DOM cargado!!");

    var formulario = document.querySelector("#formulario");
    var box_dashed = document.querySelector(".dashed");
    box_dashed.style.display = "none";

    formulario.addEventListener('submit', function(){
        console.log("Evento submit capturado");

        var nombre = document.querySelector("#nombre").value;
        var apellidos = document.querySelector("#apellidos").value;
        var edad = parseInt(document.querySelector("#edad").value);
        var correo = document.querySelector("#correo").value;

        if(nombre.trim() == null || nombre.trim().length == 0){
            document.querySelector("#error_nombre").innerHTML = "<b><strong>INGRESE UN NOMBRE VÁLIDO</strong></b>";
            return false;
        }else{
            document.querySelector("#error_nombre").style.display = "none";
        }

        if(apellidos.trim() == null || apellidos.trim().length == 0){
            document.querySelector("#error_apellido").innerHTML = "<b><strong>INGRESE UN APELLIDO VÁLIDO</strong></b>";
            return false;
        }else{
            document.querySelector("#error_apellido").style.display = "none";
        }
        console.log(edad);
        if(edad == null || edad <= 0 || isNaN(edad)){
            document.querySelector("#error_edad").innerHTML = "<b><strong>INGRESE UNA EDAD VÁLIDA</strong></b>";
            return false;
        }else{
            document.querySelector("#error_edad").style.display = "none";
        }
        if(correo.trim() == null || correo.trim().length == 0){
            document.querySelector("#error_correo").innerHTML = "<b><strong>INGRESE UN CORREO VÁLIDO</strong></b>";
            return false;
        }else{
            document.querySelector("#error_correo").style.display = "none";
        }

        box_dashed.style.display = "block";

        var p_nombre = document.querySelector("#p_nombre span");
        var p_apellidos = document.querySelector("#p_apellidos span");
        var p_edad  = document.querySelector("#p_edad span");
        var p_correo = document.querySelector("#p_correo span");

        p_nombre.innerHTML= nombre;
        p_apellidos.innerHTML= apellidos;
        p_edad.innerHTML= edad;
        p_correo.innerHTML= correo;
        
        
        /*
        var datos_usuario = [nombre, apellidos, edad, correo];

        var indice;
        for(indice in datos_usuario){
            var parrafo = document.createElement("p");
            parrafo.append(datos_usuario[indice]);
            box_dashed.append(parrafo);
        }*/

    });
});